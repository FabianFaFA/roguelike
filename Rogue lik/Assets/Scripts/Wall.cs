﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitpoint = 2; 

	void Start () {
	
	}
	  
    public void DamageWall(int damageReceived)
    {
        hitpoint -= damageReceived; 
        if(hitpoint <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
